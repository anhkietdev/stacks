﻿//Duration: 7 minutes

namespace Stack
{
    public class MyStack
    {
        int[] stack;
        int top = -1;
        int maxStackSize;
        public MyStack(int maxStackSize)
        {
            this.maxStackSize = maxStackSize;
            stack = new int[maxStackSize];
        }

        public bool IsEmpty() => top == -1;

        public bool IsFull() => top == maxStackSize - 1;

        public void Push(int item)
        {
            if (IsFull())
            {
                throw new Exception("Stack is full");
            }
            top++;
            stack[top] = item;
        }

        public int Pop()
        {
            if (IsEmpty())
            {
                throw new Exception("Stack is empty");
            }
            int topValue = stack[top];
            top--;
            return topValue;

        }

        public int Peek() //return top value but not remove
        {
            if (IsEmpty())
            {
                throw new Exception("Stack is empty");
            }
            return stack[top];
        }

        public int Count => top + 1;
    }
}
