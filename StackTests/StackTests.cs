//Duration: 12m30s

using Stack;
namespace StackTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestConstructor_CreatesEmptyStack()
        {
            // Arrange
            int maxStackSize = 10;

            // Act
            MyStack stack = new MyStack(maxStackSize);

            // Assert
            Assert.That(stack.IsEmpty, Is.True);
            Assert.That(stack.Count, Is.EqualTo(0));
        }

        [Test]
        public void TestPush_EmptyStack_AddsItem()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);
            int itemToPush = 5;

            // Act
            stack.Push(itemToPush);

            // Assert
            Assert.That(stack.IsEmpty, Is.False);
            Assert.That(stack.Count, Is.EqualTo(1));
            Assert.That(stack.Peek(), Is.EqualTo(itemToPush));
        }

        [Test]
        public void TestPush_MultipleItems_AddsInOrder()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);
            int[] itemsToPush = { 1, 2, 3 };

            // Act
            foreach (int item in itemsToPush)
            {
                stack.Push(item);
            }

            // Assert
            Assert.That(stack.IsEmpty, Is.False);
            Assert.That(stack.Count, Is.EqualTo(itemsToPush.Length));

            int expectedTop = itemsToPush.Length - 1;
            for (int i = expectedTop; i >= 0; i--)
            {
                Assert.That(stack.Pop(), Is.EqualTo(itemsToPush[i]));
            }
        }

        [Test]
        public void TestPush_FullStack_ThrowsException()
        {
            // Arrange
            int maxStackSize = 1;
            MyStack stack = new MyStack(maxStackSize);
            stack.Push(1);

            // Act (should throw exception)
            stack.Push(2);
        }

        [Test]
        public void TestPop_EmptyStack_ThrowsException()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);

            // Act (should throw exception)
            Assert.That(() => stack.Pop(), Throws.Exception);
        }

        [Test]
        public void TestPop_MultipleItems_PopsInOrder()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);
            int[] itemsToPush = { 1, 2, 3 };
            foreach (int item in itemsToPush)
            {
                stack.Push(item);
            }

            // Act
            int[] poppedItems = new int[itemsToPush.Length];
            for (int i = 0; i < itemsToPush.Length; i++)
            {
                poppedItems[i] = stack.Pop();
            }

            // Assert
            Assert.That(poppedItems, Is.EqualTo(itemsToPush.Reverse()));
        }

        [Test]
        public void TestPeek_EmptyStack_ThrowsException()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);

            // Act (should throw exception)
            Assert.That(() => stack.Peek(), Throws.Exception);
        }

        [Test]
        public void TestPeek_SingleItem_ReturnsTopWithoutRemoval()
        {
            // Arrange
            int maxStackSize = 10;
            MyStack stack = new MyStack(maxStackSize);
            int itemToPush = 5;
            stack.Push(itemToPush);

            // Act
            int peekedValue = stack.Peek();

            // Assert
            Assert.False(stack.IsEmpty(), "Stack should not be empty after peek");
            Assert.AreEqual(itemToPush, peekedValue);
            Assert.AreEqual(1, stack.Count);
        }
    }
}